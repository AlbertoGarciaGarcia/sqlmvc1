package UD22MVC.MySqlEjercicio1ModelServicio;

import java.sql.SQLException;

import javax.swing.JOptionPane;

import UD22MVC.MySqlEjercicio1Controller.ClienteController;
import UD22MVC.MySqlEjercicio1ModelDao.Dao;
import UD22MVC.MySqlEjercicio1ModelDto.Cliente;

public class ClienteServ {
// 
	private ClienteController clienteController;
	public static boolean modificaCliente=false;
	public void setClienteController(ClienteController ClienteController) {
		this.setClienteController(ClienteController);		
	}
// Haremos una validacion para que se tenga que poner mas de un caracter en el nombre y en el apellido 
	public static void validarRegistro(Cliente cliente) throws SQLException {
		if (cliente.getApellidoCliente().length() > 1 && cliente.getNombreCliente().length() > 1 ) {
			 Dao.insertarCliente(cliente);						
		}else {
			JOptionPane.showMessageDialog(null,"El nombre y el apellido deben tener mas de una letra","Advertencia",JOptionPane.WARNING_MESSAGE);
			
		}
		
	}
	
	
	
	public static void validarModificacion(Cliente cliente) throws SQLException {
		Dao dao;
		if (cliente.getNombreCliente().length()> 1 &&  cliente.getApellidoCliente().length() > 1) {
			dao = new Dao();
			Dao.modificarCliente(cliente);	
			modificaCliente=true;
		}else{
			JOptionPane.showMessageDialog(null,"El nombrey el apellido deben tener mas de una letra","Advertencia",JOptionPane.WARNING_MESSAGE);
			modificaCliente=false;
		}
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	public ClienteController getClienteController() {
		return clienteController;
	}
	
	public void gsetClienteController(ClienteController clienteController) {
		this.clienteController = clienteController;
	}
	
	
	
	
}
