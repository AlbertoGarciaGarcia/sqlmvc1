package UD22MVC.MySqlEjercicio1View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import UD22MVC.MySqlEjercicio1Controller.ClienteController;
import UD22MVC.MySqlEjercicio1ModelDto.Cliente;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
public class InsertarCliente extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTextField textFieldCodigo;
	private JTextField textFieldNombre;
	private JTextField textFieldApellido;
	private JLabel lblEdad;
	private JTextField textFieldDni;
	private JLabel lblDireccion;
	private JTextField textFieldDireccion;
	private JLabel lblFecha;
	private JTextField textFieldFecha;
	private JButton btnRegistrar;
	private JButton btnCancelar;
	private JLabel lblAdministrarClientes;
	private ClienteController clienteController;
	// Declaramos todas los objetos necesarios y establecemos el layout como queramos
	public InsertarCliente() {
		setTitle("Registro Personas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 646, 452);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCodigo = new JLabel("Código");
		lblCodigo.setBounds(38, 105, 56, 16);
		contentPane.add(lblCodigo);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(38, 162, 56, 16);
		contentPane.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(38, 226, 56, 16);
		contentPane.add(lblApellido);
		
		textFieldCodigo = new JTextField();
		textFieldCodigo.setBounds(98, 101, 116, 24);
		contentPane.add(textFieldCodigo);
		textFieldCodigo.setColumns(10);
		
		textFieldNombre = new JTextField();
		textFieldNombre.setColumns(10);
		textFieldNombre.setBounds(98, 159, 226, 24);
		contentPane.add(textFieldNombre);
		
		textFieldApellido = new JTextField();
		textFieldApellido.setColumns(10);
		textFieldApellido.setBounds(98, 223, 226, 24);
		contentPane.add(textFieldApellido);
		
		lblEdad = new JLabel("DNI");
		lblEdad.setBounds(356, 162, 30, 16);
		contentPane.add(lblEdad);
		
		textFieldDni = new JTextField();
		textFieldDni.setColumns(10);
		textFieldDni.setBounds(399, 159, 178, 24);
		contentPane.add(textFieldDni);
		
		lblDireccion = new JLabel("Dirección");
		lblDireccion.setBounds(38, 282, 56, 16);
		contentPane.add(lblDireccion);
		
		textFieldDireccion = new JTextField();
		textFieldDireccion.setColumns(10);
		textFieldDireccion.setBounds(98, 279, 226, 24);
		contentPane.add(textFieldDireccion);
		
		lblFecha = new JLabel("Fecha");
		lblFecha.setBounds(356, 226, 46, 16);
		contentPane.add(lblFecha);
		
		textFieldFecha = new JTextField();
		textFieldFecha.setColumns(10);
		textFieldFecha.setBounds(399, 223, 178, 24);
		contentPane.add(textFieldFecha);
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(209, 328, 109, 25);
		contentPane.add(btnRegistrar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(330, 328, 109, 25);
		contentPane.add(btnCancelar);
		
		

		btnRegistrar.addActionListener(this);
		btnCancelar.addActionListener(this);
		add(btnCancelar);
		add(btnRegistrar);
		limpiar();

		
		
		lblAdministrarClientes = new JLabel("REGISTRAR CLIENTES");
		lblAdministrarClientes.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblAdministrarClientes.setBounds(209, 40, 240, 32);
		contentPane.add(lblAdministrarClientes);
	}
	
	private void limpiar() 
	{
		textFieldCodigo.setText("");
		textFieldNombre.setText("");
		textFieldApellido.setText("");
		textFieldDireccion.setText("");
		textFieldDni.setText("");
		textFieldFecha.setText("");
	}

	public void setCoordinador(ClienteController clienteController) {
		this.clienteController=clienteController;
	}
	
@Override
	public void actionPerformed(ActionEvent e) 
	{
	// Hacemos el metodo para insertar
		if (e.getSource()==btnRegistrar)
		{
			try {
				Cliente cliente=new Cliente();
				cliente.setIdCliente(Integer.parseInt(textFieldCodigo.getText()));
				cliente.setNombreCliente(textFieldNombre.getText());
				cliente.setApellidoCliente(textFieldApellido.getText());
				cliente.setDireccionCliente(textFieldDireccion.getText());
				cliente.setDniCliente(textFieldDni.getText());
				cliente.setfechaCliente(textFieldFecha.getText());
				
				clienteController.insertarCliente(cliente);
				
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null,"Error en el Ingreso de Datos","Error",JOptionPane.ERROR_MESSAGE);
				System.out.println(ex);
			}
		}
		// Cerramos la ventana
		if (e.getSource()==btnCancelar)
		{
			this.dispose();
		}
	}


}
