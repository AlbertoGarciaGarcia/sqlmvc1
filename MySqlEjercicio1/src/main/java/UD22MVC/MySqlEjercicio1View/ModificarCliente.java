package UD22MVC.MySqlEjercicio1View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import UD22MVC.MySqlEjercicio1Controller.ClienteController;
import UD22MVC.MySqlEjercicio1ModelDto.Cliente;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
public class ModificarCliente extends JFrame implements ActionListener{

	private JPanel contentPane;
	private JTextField textFieldCodigo;
	private JTextField textFieldNombre;
	private JTextField textFieldApellido;
	private JLabel lblEdad;
	private JTextField textFieldDni;
	private JLabel lblDireccion;
	private JTextField textFieldDireccion;
	private JLabel lblFecha;
	private JTextField textFieldFecha;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JButton btnOk;
	private JButton btnCancelar;
	private JLabel lblAdministrarClientes;
	private ClienteController clienteController;

// Declaramos todas los objetos necesarios y establecemos el layout como queramos
	public ModificarCliente() {
		setTitle("Registro clientes");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 646, 452);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCodigo = new JLabel("Código");
		lblCodigo.setBounds(38, 105, 56, 16);
		contentPane.add(lblCodigo);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(38, 162, 56, 16);
		contentPane.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(38, 226, 56, 16);
		contentPane.add(lblApellido);
		
		textFieldCodigo = new JTextField();
		textFieldCodigo.setBounds(98, 101, 116, 24);
		contentPane.add(textFieldCodigo);
		textFieldCodigo.setColumns(10);
		
		textFieldNombre = new JTextField();
		textFieldNombre.setColumns(10);
		textFieldNombre.setBounds(98, 159, 226, 24);
		contentPane.add(textFieldNombre);
		
		textFieldApellido = new JTextField();
		textFieldApellido.setColumns(10);
		textFieldApellido.setBounds(98, 223, 226, 24);
		contentPane.add(textFieldApellido);
		
		lblEdad = new JLabel("DNI");
		lblEdad.setBounds(356, 162, 30, 16);
		contentPane.add(lblEdad);
		
		textFieldDni = new JTextField();
		textFieldDni.setColumns(10);
		textFieldDni.setBounds(399, 159, 178, 24);
		contentPane.add(textFieldDni);
		
		lblDireccion = new JLabel("Dirección");
		lblDireccion.setBounds(38, 282, 56, 16);
		contentPane.add(lblDireccion);
		
		textFieldDireccion = new JTextField();
		textFieldDireccion.setColumns(10);
		textFieldDireccion.setBounds(98, 279, 226, 24);
		contentPane.add(textFieldDireccion);
		
		lblFecha = new JLabel("Fecha");
		lblFecha.setBounds(356, 226, 46, 16);
		contentPane.add(lblFecha);
		
		textFieldFecha = new JTextField();
		textFieldFecha.setColumns(10);
		textFieldFecha.setBounds(399, 223, 178, 24);
		contentPane.add(textFieldFecha);
		
		 btnOk = new JButton("Ok");
		btnOk.setBounds(227, 101, 73, 25);
		contentPane.add(btnOk);
		
		btnModificar = new JButton("Modificar");
		btnModificar.setBounds(98, 326, 109, 25);
		contentPane.add(btnModificar);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(368, 326, 109, 25);
		contentPane.add(btnEliminar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(227, 326, 109, 25);
		contentPane.add(btnCancelar);
		
		lblAdministrarClientes = new JLabel("ADMINISTRAR CLIENTES");
		lblAdministrarClientes.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblAdministrarClientes.setBounds(199, 40, 240, 32);
		contentPane.add(lblAdministrarClientes);
		btnModificar.addActionListener(this);
		btnEliminar.addActionListener(this);
		btnCancelar.addActionListener(this);
		btnOk.addActionListener(this);
		add(btnModificar);
		add(btnEliminar);
		add(btnCancelar);
		add(btnOk);
		
		// Añadimos listeners a los botones que utilizaremos
		
		
		
		
	}

	public void setCoordinador(ClienteController clienteController) {
		this.clienteController=clienteController;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		// Dependiendo de la accion, el boton llamara al metodo necesario, en este caso al buscar cliente
		
		if (e.getSource()==btnOk)
		{
			Cliente cliente;
			try {
				cliente = ClienteController.buscarCliente(Integer.parseInt(textFieldCodigo.getText()));
				
				if (cliente!=null)
				{
					muestraPersona(cliente);
				}
				else {
					System.out.println("a");
				}
			} catch (NumberFormatException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
		
		}
		
		
		
		if (e.getSource()==btnCancelar)
		{
			this.dispose();
		}
		
		
		
		
		
		
		
		// Aqui pasaremos los valores al dto mediante el modificar
		if (e.getSource()==btnModificar)
		{
			try {
				Cliente cliente=new Cliente();
				cliente.setIdCliente(Integer.parseInt(textFieldCodigo.getText()));
				cliente.setNombreCliente(textFieldNombre.getText());
				cliente.setApellidoCliente(textFieldApellido.getText());
				cliente.setDireccionCliente(textFieldDireccion.getText());
				cliente.setDniCliente(textFieldDni.getText());
				cliente.setfechaCliente(textFieldFecha.getText());

				ClienteController.modificarCliente(cliente);
				
				
			} catch (Exception e2) {
				JOptionPane.showMessageDialog(null,"Error en el Ingreso de Datos","Error",JOptionPane.ERROR_MESSAGE);
			}
	
		}
		
		// Haremos una verficicaion para eliminar
		if (e.getSource()==btnEliminar)
		{
			if (!textFieldCodigo.getText().equals(""))
			{
				int respuesta = JOptionPane.showConfirmDialog(this,
						"Esta seguro de eliminar la Persona?", "Confirmación",
						JOptionPane.YES_NO_OPTION);
				if (respuesta == JOptionPane.YES_NO_OPTION)
				{
					try {
						ClienteController.eliminarCliente(textFieldCodigo.getText());
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					limpiar();
				}
			}
	
		}
		
		
	}
	
	// Llamamos a los getters del dao para poner los parametros dependiendo de la id
	private void muestraPersona(Cliente cliente) {
		textFieldNombre.setText(cliente.getNombreCliente());
		textFieldApellido.setText(cliente.getApellidoCliente()+"");
		textFieldDireccion.setText(cliente.getDireccionCliente()+"");
		textFieldDni.setText(cliente.getDniCliente());
		textFieldFecha.setText(cliente.getfechaCliente());

	}

	
	
	// Vaciamos 
	public void limpiar()
	{
		textFieldCodigo.setText("");
		textFieldNombre.setText("");
		textFieldApellido.setText("");
		textFieldDireccion.setText("");
		textFieldDni.setText("");
		textFieldFecha.setText("");
	}

	
	
	
	
	
	
	
	
	

}
