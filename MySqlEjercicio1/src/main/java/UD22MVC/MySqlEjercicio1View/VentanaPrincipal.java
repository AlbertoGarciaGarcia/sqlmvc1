package UD22MVC.MySqlEjercicio1View;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import UD22MVC.MySqlEjercicio1Controller.ClienteController;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class VentanaPrincipal extends JFrame implements ActionListener{

	private JPanel contentPane;
	private ClienteController clienteController;
	private JButton btnBuscar, btnRegistrarClientes;
		
	
	public VentanaPrincipal() {
		setTitle("Principal");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 601, 275);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPrincipal = new JLabel("Que acción quieres realizar?");
		lblPrincipal.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblPrincipal.setBounds(164, 53, 239, 31);
		contentPane.add(lblPrincipal);
		
		 btnBuscar = new JButton("Buscar");
		btnBuscar.setBounds(56, 144, 97, 25);
		contentPane.add(btnBuscar);
		
		 btnRegistrarClientes = new JButton("Registrar clientes");
		btnRegistrarClientes.setBounds(341, 144, 163, 25);
		contentPane.add(btnRegistrarClientes);
		
		btnBuscar.addActionListener(this);
		btnRegistrarClientes.addActionListener(this);
		getContentPane().add(btnBuscar);
		getContentPane().add(btnRegistrarClientes);
		setVisible(true);
		

	}
	
	public void setCoordinador(ClienteController clienteController) {
		this.clienteController=clienteController;
	}
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==btnRegistrarClientes) {
			clienteController.mostrarInsertarCliente();			
		}
		if (e.getSource()==btnBuscar) {
			clienteController.mostrarModificarCliente();			
		}
	}

	

}
